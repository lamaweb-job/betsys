<?php

require __DIR__.'/vendor/autoload.php';

define('TEMP_DIR', __DIR__ . '/temp');
Tracy\Debugger::$showLocation = true;

$loader = new Nette\Loaders\RobotLoader;
$loader->addDirectory(__DIR__ . '/app');
$loader->setTempDirectory(TEMP_DIR);
$loader->register();