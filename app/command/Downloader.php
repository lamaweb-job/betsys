<?php

namespace App\Command;

use App\Model\Counter\Counter;
use App\Model\Saver\Saver;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Downloader extends Command
{
//https://jsonplaceholder.typicode.com/users
//https://jsonplaceholder.typicode.com/posts
//https://jsonplaceholder.typicode.com/comments
    protected static $defaultName = 'app:downloader';

    protected function configure(bool $requirePassword = false)
    {
        $this
            ->setDescription('Creates a new user.')
            ->setHelp('This command allows you to create a user...')
            ->addArgument('export', InputArgument::OPTIONAL, 'Types to save split by comma', 'html,json,xml')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('Starting Downloader!');

        $output->writeln('Downloading json data!');
        $rawUsers = $this->getJson('https://jsonplaceholder.typicode.com/users');
        $rawPosts = $this->getJson('https://jsonplaceholder.typicode.com/posts');
        $rawComments = $this->getJson('https://jsonplaceholder.typicode.com/comments');

        $output->writeln('Running processing data!');
        $counter = new Counter();
        $counter->setUsers(json_decode($rawUsers));
        $counter->setPosts(json_decode($rawPosts));
        $counter->setComments(json_decode($rawComments));

        $output->writeln('Exporting data!');
        $exports = $this->getExportTypes($input);
        $saver = new Saver($output, $exports, $counter);
        $saver->loadSavers();
        $saver->runSaving();

        $output->writeln('Finished Downloader!');
    }

    /**
     * load data from json from url
     * @param string $jsonUrl
     * @return string
     */
    protected function getJson(string $jsonUrl): string
    {
        $ch = curl_init( $jsonUrl );
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
        );
        curl_setopt_array( $ch, $options );
        return curl_exec($ch);
    }

    /**
     * @param InputInterface $input
     * @return string[]
     */
    protected function getExportTypes(InputInterface $input): array
    {
        return explode(',', $input->getArgument('export'));
    }
}