<?php
namespace App\Model\Saver\Type;

interface IType
{
    /**
     * method to create specific type od result
     * @param $path
     * @param array $users
     *
     * @return string
     */
    public function generate(array $users): string;

    /**
     * return file type
     * @return string
     */
    public function getFileType(): string;
}