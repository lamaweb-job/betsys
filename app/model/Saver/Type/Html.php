<?php

namespace App\Model\Saver\Type;

use Latte\Engine;

class Html implements IType
{
    public function generate(array $users): string
    {
        $latte = new Engine();
        return $latte->renderToString(__DIR__ . '/html.template.latte', ['users' => $users]);
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getFileType(): string
    {
        return 'html';
    }
}