<?php

namespace App\Model\Saver\Type;

class Json implements IType
{
    public function generate(array $users): string
    {
        return 'TODO Vratilo by JSON strukturu ve stringu';
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getFileType(): string
    {
        return 'json';
    }
}