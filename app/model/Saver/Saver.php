<?php

namespace App\Model\Saver;


use App\Model\Counter\Counter;
use App\Model\Saver\Type\IType;
use Symfony\Component\Console\Output\OutputInterface;

class Saver
{
    /** @var string[] */
    protected $exportTypes = [];
    /** @var Counter */
    protected $counter;
    /** @var IType[] */
    protected $savers = [];
    /** @var OutputInterface */
    protected $output;


    public function __construct(OutputInterface $output, array $exportTypes, Counter $counter)
    {
        $this->exportTypes = $exportTypes;
        $this->counter = $counter;
        $this->output = $output;
    }

    public function loadSavers(): void
    {
        foreach ($this->exportTypes AS $exportType){
            $saver = $this->getSaver($exportType);
            if($saver){
                $this->savers[] = $saver;
            }
        }
    }

    protected function getSaver($exportType):? IType
    {
        $className = 'App\Model\Saver\Type\\' . ucfirst(strtolower($exportType));
        if(class_exists($className)){
            return new $className();
        }
        return null;
    }

    public function runSaving(): void
    {
        foreach ($this->savers AS $saver){
            $this->output->writeln('Saving: ' . $saver->getFileType());
            $string = $saver->generate($this->counter->getUsers());
            $filePath = $this->getFilePath($saver->getFileType());
            $this->createFile($filePath, $string);
        }
    }

    protected function getFilePath($fileType): string
    {
        return TEMP_DIR . '/' . $fileType . '-' . time() . '.' . $fileType;
    }

    protected function createFile($filePath, $string): bool
    {
        $fp = fopen($filePath, 'w');
        fwrite($fp, $string);
        return fclose($fp);
    }



}