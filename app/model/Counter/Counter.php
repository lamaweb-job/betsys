<?php

namespace App\Model\Counter;


use App\Model\Counter\Type\Address;
use App\Model\Counter\Type\Comment;
use App\Model\Counter\Type\Company;
use App\Model\Counter\Type\Geo;
use App\Model\Counter\Type\Post;
use App\Model\Counter\Type\User;

class Counter
{
    /** @var User[]  */
    protected $users = [];
    /** @var Post[]  */
    protected $posts = [];
    /** @var Comment[] */
    protected $comments = [];

    public function setUsers(array $rawUsers): void
    {
        $this->users = [];
        foreach ($rawUsers AS $rawUser){
            $this->users[$rawUser->id] = new User(
                $rawUser->id,
                $rawUser->name,
                $rawUser->username,
                $rawUser->email,
                $this->getAddressFromUser($rawUser->address),
                $rawUser->phone,
                $rawUser->website,
                $this->getCompanyFromUser($rawUser->company)
            );
        }
    }

    public function setPosts(array $rawPosts): void
    {
        $this->posts = [];
        foreach ($rawPosts AS $rawPost){
            $this->posts[$rawPost->id] = $post = new Post(
                (int) $rawPost->id,
                (int) $rawPost->userId,
                $rawPost->title,
                $rawPost->body
            );
            $user = $this->getUser((int)$rawPost->userId);
            if($user){
                $user->addPost($post);
            }
        }
    }

    public function setComments(array $rawComments): void
    {
        $this->comments = [];
        foreach ($rawComments AS $rawComment){
            $this->comments[$rawComment->id] = $comment = new Comment(
                (int) $rawComment->id,
                (int) $rawComment->postId,
                $rawComment->name,
                $rawComment->email,
                $rawComment->body
            );
            $post = $this->getPost((int)$rawComment->postId);
            if($post){
                $post->addComment($comment);
            }
        }
    }

    public function getAddressFromUser(\stdClass $address)
    {
        return new Address($address->street, $address->suite, $address->city, $address->zipcode, new Geo($address->geo->lat, $address->geo->lng));
    }

    public function getCompanyFromUser(\stdClass $company)
    {
        return new Company($company->name, $company->catchPhrase, $company->bs);
    }

    /**
     * @param $userId
     * @return User|null
     */
    protected function getUser($userId):? User
    {
        if(isset($this->users[$userId])){
            return $this->users[$userId];
        }
        return null;
    }

    /**
     * @param $postId
     * @return Post|null
     */
    protected function getPost($postId):? Post
    {
        if(isset($this->posts[$postId])){
            return $this->posts[$postId];
        }
        return null;
    }

    /**
     * @return User[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * @return Post[]
     */
    public function getPosts(): array
    {
        return $this->posts;
    }

    /**
     * @return Comment[]
     */
    public function getComments(): array
    {
        return $this->comments;
    }
}