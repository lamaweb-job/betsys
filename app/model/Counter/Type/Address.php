<?php

namespace App\Model\Counter\Type;


class Address
{
    /** @var string */
    private $street;
    /** @var string */
    private $suite;
    /** @var string */
    private $city;
    /** @var string */
    private $zipcode;
    /** @var Geo */
    private $geo;

    /**
     * Address constructor.
     * @param string $street
     * @param string $suite
     * @param string $city
     * @param string $zipcode
     * @param Geo $geo
     */
    public function __construct(string $street, string $suite, string $city, string $zipcode, Geo $geo)
    {
        $this->street = $street;
        $this->suite = $suite;
        $this->city = $city;
        $this->zipcode = $zipcode;
        $this->geo = $geo;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getSuite(): string
    {
        return $this->suite;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getZipcode(): string
    {
        return $this->zipcode;
    }

    /**
     * @return Geo
     */
    public function getGeo(): Geo
    {
        return $this->geo;
    }
}