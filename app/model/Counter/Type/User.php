<?php

namespace App\Model\Counter\Type;


class User
{
    /** @var int  */
    private $id;
    /** @var string  */
    private $name;
    /** @var string  */
    private $username;
    /** @var string  */
    private $email;
    /** @var Address */
    private $address;
    /** @var string  */
    private $phone;
    /** @var string  */
    private $website;
    /** @var Company */
    private $company;

    private $postsCount = 0;
    /** @var Post[] */
    private $posts = [];

    public function __construct(int $id, string $name, string $username, string $email, Address $address, string $phone, string $website, Company $company)
    {
        $this->id = $id;
        $this->name = $name;
        $this->username = $username;
        $this->email = $email;
        $this->address = $address;
        $this->phone = $phone;
        $this->website = $website;
        $this->company = $company;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getWebsite(): string
    {
        return $this->website;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @return int
     */
    public function getPostsCount(): int
    {
        return $this->postsCount;
    }

    /**
     * @param int $postsCount
     */
    public function setPostsCount(int $postsCount): void
    {
        $this->postsCount = $postsCount;
    }

    /**
     * @return Post[]
     */
    public function getPosts(): array
    {
        return $this->posts;
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post): void
    {
        $this->posts[] = $post;
        $this->setPostsCount(\count($this->posts));
    }
}