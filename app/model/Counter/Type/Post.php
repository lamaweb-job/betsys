<?php

namespace App\Model\Counter\Type;


class Post
{
    /** @var int */
    private $id;
    /** @var int */
    private $userId;
    /** @var string */
    private $title;
    /** @var string */
    private $body;

    /** @var int */
    private $commentsCount = 0;
    /** @var Comment[] */
    private $comments = [];


    /**
     * Post constructor.
     * @param int $id
     * @param int $userId
     * @param string $title
     * @param string $body
     */
    public function __construct(int $id, int $userId, string $title, string $body)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->title = $title;
        $this->body = $body;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return int
     */
    public function getCommentsCount(): int
    {
        return $this->commentsCount;
    }

    /**
     * @param int $commentsCount
     */
    public function setCommentsCount(int $commentsCount): void
    {
        $this->commentsCount = $commentsCount;
    }

    /**
     * @return Comment[]
     */
    public function getComments(): array
    {
        return $this->comments;
    }

    /**
     * @param Comment $comment
     */
    public function addComment(Comment $comment): void
    {
        $this->comments[] = $comment;
        $this->setCommentsCount(\count($this->comments));
    }
}