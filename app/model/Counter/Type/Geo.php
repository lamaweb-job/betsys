<?php

namespace App\Model\Counter\Type;


class Geo
{
    /** @var float */
    private $lan;
    /** @var float */
    private $lng;

    /**
     * Geo constructor.
     * @param float $lan
     * @param float $lng
     */
    public function __construct(float $lan, float $lng)
    {
        $this->lan = $lan;
        $this->lng = $lng;
    }

    /**
     * @return float
     */
    public function getLan(): float
    {
        return $this->lan;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }
}