<?php

namespace App\Model\Counter\Type;


class Company
{
    /** @var string */
    private $name;
    /** @var string */
    private $catchPhrase;
    /** @var string */
    private $bs;

    /**
     * Company constructor.
     * @param string $name
     * @param string $catchPhrase
     * @param string $bs
     */
    public function __construct(string $name, string $catchPhrase, string $bs)
    {
        $this->name = $name;
        $this->catchPhrase = $catchPhrase;
        $this->bs = $bs;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCatchPhrase(): string
    {
        return $this->catchPhrase;
    }

    /**
     * @return string
     */
    public function getBs(): string
    {
        return $this->bs;
    }
}