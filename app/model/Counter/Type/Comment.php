<?php

namespace App\Model\Counter\Type;


class Comment
{
    /** @var int */
    private $id;
    /** @var int */
    private $postId;
    /** @var string */
    private $name;
    /** @var string */
    private $email;
    /** @var string */
    private $body;

    /**
     * Comment constructor.
     * @param int $id
     * @param int $postId
     * @param string $name
     * @param string $email
     * @param string $body
     */
    public function __construct(int $id, int $postId, string $name, string $email, string $body)
    {
        $this->id = $id;
        $this->postId = $postId;
        $this->name = $name;
        $this->email = $email;
        $this->body = $body;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPostId(): int
    {
        return $this->postId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }
}