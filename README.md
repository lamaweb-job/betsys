# Task
## Non-functional requirements:

  1. Easy to setup and run. If possible prepare the code in a docker container for easier setup. 
    Describe setup procedure and usage.

  2. Don't use frameworks. You can use external libraries, though.

  3. Code should be easy to maintain, extend and modify (OOP, SOLID, Clean Code, KISS, 
    DRY, separation of concerns).

  4. If you have experience with tests, then write some.

  5. Code does not need to be 100% complete or working. We value your time and if you 
    cannot finish the task please just send what you have (consider having empty/fake interface 
    implementations to depict your ideas; also,describe what would you do/change if you had 
    more time).

  6. Add comments/descriptions about used solutions if you think this is appropriate.

  7. You have 7 days to send us your code but don't treat it as a dead-line. We just don't want 
    you to spend too much time on it. If you need more time just let us know.

  8. Code has to be written with PHP at least 5.6. If you know PHP 7 feel free to solve it with it.

  9. Remember to add proper error handling.

  10. All comments, descriptions and code must be in English.

The task is very simple but we want to see what you can do so don't worry about 
"over-engineering" it. That is the point :)

## Functional requirements:
Please write CLI application that reads a list of posts, comments and users from remote API 
(https://jsonplaceholder.typicode.com/) and store count of relations and „entity" informations . The 
application must be able to store result in any (or all) of the following formats: HTML, JSON, 
XML. The application should be able to store all formats during one execution, if specified. The 
application must have the parameters for whom and what we want to count.

## Example:
$ # store infromations about post with id 1 and count of comments for post with id 1 
$ ./example post 1 comment
$ # store informations about user with id 2 and count of posts for user with id 2 
$ ./example user 2 post

## Processing
### Creating of docker containers
For creating docker containers run
```bash 
docker-compose up --build -d
docker-compose exec betsys-php composer install
```
For running script use
```bash
docker-compose exec betsys-php php index.php app:downloader 
```
If you want to decide which type should be used command is:
```bash
docker-compose exec betsys-php php index.php app:downloader html,xml
```
#### Target folder
Because no specific path needed, I save files in temp folder.

### Missing parts
#### Json & XML Savers
If I would have more time I would use on object `App\Model\Counter\Type\*` some ArrayIterator, for passing data from `Object` to `array`.
But I did not use anyone until, so I do not have that much experience in this time.

#### Testing
Even I use in past some PHPUnit, selenium and PHPStan tests, In short time is hard for me to set up properly, so I decided to focus on main structure. 

#### Specific parameter
I focus on processing all downloaded data from JSON repositories, but it is possible to takes only part, but I decided to not creating all possible combination with given time I decide to spend on the task.

### Time spend
Totally I spend on core programing of app about 4 hours, partly on learning each used library, partly on creating data structure.  
 